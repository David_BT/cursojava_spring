package br.com.cursojava.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

public class ValidateExampleAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping,ActionForm form,
			HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		ActionMessages messages = new ActionMessages();
		messages.add("success", new ActionMessage("validateExample.success"));
		
		saveMessages(request, messages);
		return mapping.findForward("default");
	}
	
}
