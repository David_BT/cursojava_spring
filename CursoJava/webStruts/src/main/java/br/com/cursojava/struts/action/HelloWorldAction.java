package br.com.cursojava.struts.action;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import br.com.cursojava.struts.form.HelloWorldForm;

public class HelloWorldAction extends DispatchAction implements Serializable {

	private static final long serialVersionUID = 1L;

	public ActionForward salve(ActionMapping mapping,ActionForm form,
			HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		HelloWorldForm helloWorldForm = (HelloWorldForm) form;
		helloWorldForm.setName("Salve Galera!!");
		
		return mapping.findForward("success");
	}
	
	public ActionForward ola(ActionMapping mapping,ActionForm form,
			HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		HelloWorldForm helloWorldForm = (HelloWorldForm) form;
		helloWorldForm.setName("Galera!!");
		
		return mapping.findForward("success");
	}
	
	
	
}
