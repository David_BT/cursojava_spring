package br.com.cursojava.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;


public class TesteVariaveisAction extends DispatchAction {

	public ActionForward init(ActionMapping mapping,ActionForm form,
			HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		return mapping.findForward("default");
	}
	
	public ActionForward sessionTest(ActionMapping mapping,ActionForm form,
			HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		request.getSession().setAttribute("variavelSession", "Esta é uma variável de sessão");
		
		return mapping.findForward("session");
	}
	
	public ActionForward requestTest(ActionMapping mapping,ActionForm form,
			HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		request.setAttribute("variavelRequest", "Esta é uma variável de request");
		
		return mapping.findForward("request");
	}
	
}
