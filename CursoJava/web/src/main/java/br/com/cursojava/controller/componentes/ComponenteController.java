package br.com.cursojava.controller.componentes;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Component;

import br.com.cursojava.controller.GenericController;

@Component
@ManagedBean
@SessionScoped
public class ComponenteController extends GenericController implements Serializable{

	private static final long serialVersionUID = 8622740626358574036L;
	private String cep;
	private String endereco;
	
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
		if ("06850410".equals(cep)) {
			endereco = "Rua Salvador - 76";
		}
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
}
