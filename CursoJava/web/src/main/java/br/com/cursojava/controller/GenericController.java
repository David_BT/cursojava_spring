package br.com.cursojava.controller;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public abstract class GenericController{

	
	protected void addErrorMessage(String title, String message) {
		getCurrentInstance().addMessage(null, createFacesMessage(FacesMessage.SEVERITY_ERROR, title, message));
	}
	
	protected void addWarningMessage(String title, String message) {
		getCurrentInstance().addMessage(null, createFacesMessage(FacesMessage.SEVERITY_WARN, title, message));
	}
	
	protected void addSuccessMessage(String title, String message) {
		getCurrentInstance().addMessage(null, createFacesMessage(FacesMessage.SEVERITY_INFO, title, message));
	}

	protected FacesMessage createFacesMessage(Severity severity, String title, String message) {
		return new FacesMessage(severity, title, message);
	}

	protected FacesContext getCurrentInstance() {
		return FacesContext.getCurrentInstance();
	}
	
	
}
