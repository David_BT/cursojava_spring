package br.com.cursojava.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.springframework.stereotype.Component;

@Component
@ManagedBean
@RequestScoped
public class HelloHorld implements Serializable {

	private static final long serialVersionUID = -5682639913485620244L;

	private String nome;
	private String message;
	
	public void hello() {
		message = "Olá " + nome + ", como vai??";
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
