package br.com.cursoJava.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.cursoJava.domain.Client;

public interface ClientRepository extends JpaRepository<Client, Integer> {

	@Query("select count(c) from Client c where c.documentNumber = ?1")
	public Integer countClientByDocumentNumber(String cpf);
	
	public List<Client> findByNameContaining(String name);
}
