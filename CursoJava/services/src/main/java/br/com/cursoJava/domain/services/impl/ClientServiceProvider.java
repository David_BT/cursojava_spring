package br.com.cursoJava.domain.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.cursoJava.domain.Client;
import br.com.cursoJava.domain.exceptions.InvalidDocumentNumberException;
import br.com.cursoJava.domain.exceptions.ClientException;
import br.com.cursoJava.domain.exceptions.ClientAlreadyExistsException;
import br.com.cursoJava.domain.exceptions.ClientUnderageException;
import br.com.cursoJava.domain.services.ClientService;
import br.com.cursoJava.domain.services.impl.cliente.filtro.ClientSearchFilter;
import br.com.cursoJava.repository.ClientRepository;
import br.com.cursoJava.utils.DateUtils;

@Service
@Transactional
public class ClientServiceProvider implements ClientService {

	private static final long serialVersionUID = -1L;
	
	@Autowired
	private ClientRepository clientRepository;
	
	public void insertClient(Client client) throws ClientException {
		this.validateCliente(client, false);
		this.clientRepository.save(client);
	}

	private void clientAlreadyExists(Client client) throws ClientException {
		boolean found = this.clientRepository.countClientByDocumentNumber(client.getDocumentNumber()) > 0;
		if (found) {
			throw new ClientAlreadyExistsException();
		}
	}

	private void validateCliente(Client client,boolean isUpdate) throws ClientException{
		if (DateUtils.yearsBetween(client.getBirthdayDate(), new Date()).compareTo(18l) < 0) {
			throw new ClientUnderageException();
		}
		if ("11111111111".equals(client.getDocumentNumber())) {
			throw new InvalidDocumentNumberException();
		}
		if (!isUpdate) {
			this.clientAlreadyExists(client);
		}
		
	}

	public void deleteClient(Client cliente) {
		this.clientRepository.delete(cliente);
	}

	public void updateClient(Client client) throws ClientException {
		validateCliente(client, true);
		this.clientRepository.save(client);
	}

	public List<Client> listClients() {
		return this.clientRepository.findAll();
	}

	public List<Client> searchClients(ClientSearchFilter filter) {
		List<Client> res = this.clientRepository.findByNameContaining(filter.getName());
		return res;
	}

	public ClientRepository getClientRepository() {
		return clientRepository;
	}

	public void setClientRepository(ClientRepository clientRepository) {
		this.clientRepository = clientRepository;
	}

}
