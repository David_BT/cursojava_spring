package br.com.cursoJava.domain.services.impl.cliente.filtro;

import java.io.Serializable;

public class ClientSearchFilter implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
