package br.com.cursoJava.domain.exceptions;

public abstract class ClientException extends Exception {

	private static final long serialVersionUID = 1L;

	public abstract String getMessage();
	
}
