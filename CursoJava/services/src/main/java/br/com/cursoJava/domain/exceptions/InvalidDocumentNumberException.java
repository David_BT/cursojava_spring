package br.com.cursoJava.domain.exceptions;

public class InvalidDocumentNumberException extends ClientException {

	private static final long serialVersionUID = -3052811279206511896L;

	@Override
	public String getMessage() {
		return "O CPF do cliente deve ser válido";
	}

}
